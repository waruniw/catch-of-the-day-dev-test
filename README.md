CatchOfTheDay Developer Exam
=======

To help us understand you as a developer, we've designed this simple exam. It's an incomplete single-page TODO list application. We've placed `// TODO`
comments in the code where we believe it is incomplete.

To keep things simple, this was built on top of Symfony 3.1 Standard Edition. At a minimum, these are the steps required to run the application on your
machine.
- Download Composer (https://getcomposer.org/download/)
- `composer install`
- `bin/console assets:install`
- `bin/console server:start`


SetUp Instructions
==================
1. git clone the  repository
2. Run php composer.phar install to install packages
3. Give nessary foler permissions for var folder inside the directory. sudo chown -R ${USER}:www-data var sudo chmod -R 7555 var/
    3.1. Give write permission  to www-data to the src/CatchOfTheDay/DevExamBundle/Resources/config/next_item_id.yml file
4. Clear the cache by running php bin/console cache:clear -e prod
5. Create a virtual host for your project folder This is an example

~~~~
<VirtualHost *:80> 

 ServerName local.dev.test

 ServerAdmin webmaster@localhost
 DocumentRoot /var/www/html/catch-of-the-day-dev-test/web

 # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
 # error, crit, alert, emerg.
 # It is also possible to configure the loglevel for particular
 # modules, e.g.
 #LogLevel info ssl:warn

 ErrorLog ${APACHE_LOG_DIR}/error.log
 CustomLog ${APACHE_LOG_DIR}/access.log combined

 # For most configuration files from conf-available/, which are
 # enabled or disabled at a global level, it is possible to
 # include a line for only one particular virtual host. For example the
 # following line enables the CGI configuration for this host only
 # after it has been globally disabled with "a2disconf".
 #Include conf-available/serve-cgi-bin.conf.
</VirtualHost>
~~~~

6. For unit testing
    php phpunit.phar

7. To give permissions to config and data file run 
    ./setup.sh 
