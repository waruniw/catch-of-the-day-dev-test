<?php

namespace CatchOfTheDay\DevExamBundle\Controller;

use CatchOfTheDay\DevExamBundle\Model\TodoListItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TodoListController extends Controller
{
    /**
     * @Route("/", name="uncompleted_tasks")
     * @Template
     *
     *
     * @return array
     */
    public function indexAction(Request $request)
    {

        $page   = $request->query->get('page', 1);

        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        $items   = $manager->read();

        $paginator = $this->get('ashley_dawson_simple_pagination.paginator');


        $items = array_filter($items, function($item) {
            return $item->getComplete() == false;
        });

        $paginator->setItemTotalCallback(function () use ($items) {
            return count($items);
        });

        $paginator->setSliceCallback(function ($offset, $length) use ($items) {
            return array_slice($items, $offset, $length);
        });

        $paginator = $paginator->paginate((int)$page);
        

        return [
            'items'        => $items,
            'pagination'   => $paginator,
            'currentPage'  => $page
        ];
    }

    /**
     * @Route("/completed_tasks", name="completed_tasks")
     * @Template
     *
     *
     * @return array
     */
    public function completedTasksListAction(Request $request)
    {

        $page   = $request->query->get('page', 1);

        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        $items   = $manager->read();

        $paginator = $this->get('ashley_dawson_simple_pagination.paginator');

        $items = array_filter($items, function($item) {
            return $item->getComplete() == true;
        });


        $paginator->setItemTotalCallback(function () use ($items) {
            return count($items);
        });

        $paginator->setSliceCallback(function ($offset, $length) use ($items) {
            return array_slice($items, $offset, $length);
        });

        $paginator = $paginator->paginate((int)$page);


        return [
            'items'        => $items,
            'pagination'   => $paginator,
            'currentPage'  => $page
        ];
    }


    /**
     * @Route("/add", name="add")
     * @Method("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        // user input
        $text       = strip_tags($request->request->get('todo-text'));
        $listItems  = $manager->read();


        $item     = new TodoListItem();
        $item->setId(count($listItems) + 1);
        $item->setText($text);


        array_push($listItems, $item);

        $manager->write($listItems);

        // TODO - Read the new item's text from $request, add a new TodoListItem to the collection and save.

        return $this->redirectToRoute('uncompleted_tasks');
    }

    /**
     * @Route("/items/{itemId}/edit", name="edit")
     *
     * @param Request $request
     * @param $itemId
     *
     */
    public function editAction(Request $request, $itemId)
    {
        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        $items   = $manager->read();
        $text    = strip_tags($request->request->get('todo-text'));

        array_walk($items, function($item, $i) use($itemId, $text) {

            if ($item->getId() == $itemId) {
                $item->setText($text);
            }

            $items[$i] = $item;

        });

        $manager->write($items);

        return $this->redirectToRoute('uncompleted_tasks');
    }

    /**
     * @Route("/items/{itemId}/mark-as-complete", name="mark_as_complete")
     *
     * @param Request $request
     * @param string $itemId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function markAsCompleteAction(Request $request, $itemId)
    {

        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        $items = $manager->read();

        array_walk($items, function($item, $i) use($itemId) {

            if ($item->getId() == $itemId) {
                $item->setComplete(true);
            }

            $items[$i] = $item;

        });

        $manager->write($items);
        // TODO - Look in $items for the item that matches $itemId, update it and save the collection.

        return $this->redirectToRoute('uncompleted_tasks');
    }

    /**
     * @Route("/items/{itemId}/mark-as-uncomplete", name="mark_as_uncomplete")
     *
     * @param Request $request
     * @param string $itemId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function markAsUncompleteAction(Request $request, $itemId)
    {

        $manager = $this->get('catch_of_the_day_dev_exam.manager.todo_list');
        $items = $manager->read();

        array_walk($items, function($item, $i) use($itemId) {

            if ($item->getId() == $itemId) {
                $item->setComplete(false);
            }

            $items[$i] = $item;

        });

        $manager->write($items);
        // TODO - Look in $items for the item that matches $itemId, update it and save the collection.

        return $this->redirectToRoute('completed_tasks');
    }
}
