<?php

namespace CatchOfTheDay\DevExamBundle\Model;

use Symfony\Component\Yaml\Yaml;

class TodoListItem
{
    const CONFIG_FILE = '/../Resources/config/next-item-id.yml';
    /**
     * @var string
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var string
     */
    private $text;

    /**
     * @var bool
     */
    private $complete;

    public function __construct()
    {

        $this->created  = new \DateTime();
        $this->complete = false;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $complete
     * @return TodoListItem
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;

        return $this;
    }

    /**
     * @param \DateTime $created
     * @return TodoListItem
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param string $id
     * @return TodoListItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $text
     * @return TodoListItem
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param array $data
     * @return TodoListItem
     */
    public static function fromAssocArray(array $data)
    {
        $itemList = array();

        foreach ($data as $val) {

            $item = new TodoListItem();
            $item->setId($val['id']);
            $item->setText($val['text']);
            $item->setComplete($val['complete']);
            $item->setCreated(new \DateTime($val['created']));

            array_push($itemList, $item);
        }

        return $itemList;
    }

    /**
     * @return array
     */
    public static function toAssocArray(array $listItems)
    {
        $data = [];

        if (!empty($listItems)) {

            foreach ($listItems as $item) {

                $arrItem = array(
                    'id'       => $item->getId(),
                    'text'     => $item->getText(),
                    'complete' => $item->getComplete(),
                    'created'  => $item->getCreated()->format('Y-m-d H:i:s')
                 );
                array_push($data, $arrItem);
            }
            
        }

        return $data;
    }

}