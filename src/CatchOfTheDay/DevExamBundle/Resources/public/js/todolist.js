var TodoList =  {

    init: function () {

        var self = this;
        self.handleFormSubmissions();
    },
    handleFormSubmissions: function () {

        var self = this;

        $('form').submit(function(e) {


            e.preventDefault();

            var valid = self.validateForm(this);

            var formContainer       = $(this).parent('.contact-form--container');
            var successContainer    = formContainer.siblings('.contact-form--success');
            var submittingContainer = formContainer.siblings('.contact-form--processing');
            var action              = $(this).attr('action');

            if (valid) {
                formContainer.hide();
                submittingContainer.show();
                $.post(
                    action,
                    $( this ).serialize()
                )
                    .done(function(data) {

                        if(data.error == true) {

                            $( 'input.error').removeClass('error');
                            var count = data.messages.length;

                            for(var i=0; i<count; i++) {
                                $('#contact_' + data.messages[i]['element']).addClass('error');
                            }

                        } else {
                            submittingContainer.hide();
                            successContainer.show();

                            setTimeout(function() { window.location.reload(true); }, 2500);
                        }
                    });
            }
        });
        
    },
    validateForm: function(form) {

        var singles = $(form).serializeArray(),
            me      = this,
            missing = 0;

        // Check singles
        $.each(singles, function () {
            var valid = me.validateItem(this, form);

            if (!valid) { missing++; }
        });

        return (missing == 0);

    },
    validateItem : function( item, form) {

        var me = this,
            el = $('[name="'+item.name+'"]'),
            id = el.data('id'),
            name = el.data('name');


        var result = me.validateText(item);

        var valid =  result.valid,
            err = result.msg;


        // Toggle invalid class
        me.markValidState(el, valid, err, form);

        return valid;
    },
    markValidState: function (el, valid, err, form) {
        // Toggle invalid class
        var ele = $(form).find(el);
        ele.toggleClass('invalid', !valid);


        // Toggle error message
        var parent = ele.parents('.form-group');
        parent.find('.invalid_message').remove();
        if (!valid) {
            parent.append('<div class="invalid_message">'+err+'</div>');
        }
    },
    validateText: function(item) {

        var self = this;

        if (self.validateNotEmpty(item)) {

            return {"valid": self.validateAlphaNumeric(item), "msg" : "Only allow Alpha numeric characters"};

        } else {

            return {"valid": false, "msg" : "Cannot be Empty"};
        }
    },
    validateNotEmpty: function(item) {
        return (item.value.length > 0);
    },
    validateAlphaNumeric: function (item) {
        var re = /^[a-z\d\-_\s]+$/i
        return re.test(item.value);
    }
}
