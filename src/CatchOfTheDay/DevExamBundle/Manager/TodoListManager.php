<?php

namespace CatchOfTheDay\DevExamBundle\Manager;

use CatchOfTheDay\DevExamBundle\Model\TodoListItem;
use Symfony\Component\Config\Definition\Exception\Exception;

class TodoListManager
{
    const DATA_FILE = '@CatchOfTheDayDevExamBundle/Resources/data/todo-list.json';

    /**
     * @var \Symfony\Component\Config\FileLocatorInterface
     */
    private $fileLocator;
    private $logger;
    private $kernel;
    private $path;

    /**
     * @param \Symfony\Component\Config\FileLocatorInterface $fileLocator
     */
    public function __construct($fileLocator, $kernel)
    {
        $this->kernel      = $kernel;
        $this->fileLocator = $fileLocator;

        \Logger::configure($this->kernel->getRootDir().'/../log4php.xml');
        $this->logger = \Logger::getLogger("default");

        $this->setDataFilePath(self::DATA_FILE);

    }

    /**
     * @return string
     */
    public function getDataFilePath()
    {
        return $this->fileLocator->locate($this->path);
    }

    public function setDataFilePath($path)
    {
        $this->path = $path;
    }

    /**
     * @return \CatchOfTheDay\DevExamBundle\Model\TodoListItem[]
     */
    public function read()
    {
            try {

                $jsonFile = $this->getDataFilePath();
                $content  = file_get_contents($jsonFile);
                $data     = json_decode($content, true);
                $itemList = TodoListItem::fromAssocArray($data);

                return $itemList;

            } catch (\Exception $ex) {

                $this->logger->error($ex->getMessage());
                return array();
            }


        return array();
    }

    /**
     * @param \CatchOfTheDay\DevExamBundle\Model\TodoListItem[] $items
     */
    public function write(array $items)
    {
        try {

            $jsonFile  = $this->getDataFilePath();

            $arrayData = TodoListItem::toAssocArray($items);
            $data      = json_encode($arrayData);

            file_put_contents($jsonFile, $data);

        } catch (Exception $ex) {

            $this->logger->error($ex->getMessage());
        }

    }

}