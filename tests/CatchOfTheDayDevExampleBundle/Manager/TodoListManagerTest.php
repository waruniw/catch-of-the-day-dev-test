<?php

namespace CarOfTheDayDevExampleBundle\Tests\Manager;
use CatchOfTheDay\DevExamBundle\Model\TodoListItem;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class TodoListManagerTest extends KernelTestCase {

    const DATA_FILE = '@CatchOfTheDayDevExamBundle/Resources/data/todo-list.json';

    private $manager;

    public function setup()
    {
        $kernel       = static::createKernel();
        $kernel->boot();

        $this->manager = $kernel->getContainer()->get('catch_of_the_day_dev_exam.manager.todo_list');
        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/todo-list.json');

    }

    public function testReadEmptyFile()
    {
        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/empty-list.json');
        $data = $this->manager->read();

        $this->assertEquals(0, count($data));
    }

    public function testReadWithoutFile()
    {
        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/no-file.json');
        $data = $this->manager->read();

        $this->assertEquals(0, count($data));
    }

    public function testReadFileWithData()
    {
        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/todo-list.json');
        $data = $this->manager->read();

        $this->assertEquals(1, count($data));
        $this->assertEquals($data[0]->getText(), "Abc Ad");
        $this->assertEquals($data[0]->getId(), 1);
    }

    public function testWriteFirstRecord()
    {
        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/write-first-data.json');
        file_put_contents(__DIR__ . '/../Resources/data/write-first-data.json', []);
        $todoItem = new TodoListItem();

        $todoItem->setText("First record");
        $todoItem->setId(1);

        $data = [$todoItem];

        $beforeWrite = $this->manager->read();
        $this->assertEquals(0, count($beforeWrite));

        $this->manager->write($data);

        $results = $this->manager->read();
        $this->assertEquals(1, count($results));
        $this->assertEquals($data[0]->getText(), "First record");
        $this->assertEquals($data[0]->getId(), 1);

    }

    public function testWriteManyRecords()
    {

        $this->manager->setDataFilePath(__DIR__ . '/../Resources/data/write-data.json');
        file_put_contents(__DIR__ . '/../Resources/data/write-data.json', []);

        $item1 = new TodoListItem();
        $item1->setText("First record");
        $item1->setId(1);

        $item2 = new TodoListItem();
        $item2->setText("Second record");
        $item2->setId(2);

        $data = [$item1, $item2];

        $beforeWrite = $this->manager->read();
        $this->assertEquals(0, count($beforeWrite));

        $this->manager->write($data);

        $results = $this->manager->read();
        $this->assertEquals(2, count($results));
        $this->assertEquals($data[0]->getText(), "First record");
        $this->assertEquals($data[0]->getId(), 1);

        $this->assertEquals($data[1]->getText(), "Second record");
        $this->assertEquals($data[1]->getId(), 2);
    }


}
