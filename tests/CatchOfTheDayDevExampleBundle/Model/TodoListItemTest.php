<?php

namespace CarOfTheDayDevExampleBundle\Tests\Model;

use CatchOfTheDay\DevExamBundle\Model\TodoListItem;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\DateTime;

class TodoListItemTest extends TestCase {


    public function setup()
    {

    }

    public function testFromAssocArray()
    {
        $data = array(
            'id'        => 1,
            'text'      => "Sample",
            'created'   => "2018-01-01 00:00:00",
            'complete'  => false
        );

        $result = TodoListItem::fromAssocArray(array($data));
        $this->assertInstanceOf(TodoListItem::class, $result[0]);
        $this->assertEquals(1, $result[0]->getId());
        $this->assertClassHasAttribute('id', TodoListItem::class);
    }



    public function testToAssocArray()
    {
        $item1 = new TodoListItem();
        $item1->setId(1);
        $item1->setText("Test");

        $items = array(
            $item1
        );

        $output = TodoListItem::toAssocArray($items);
        $this->assertEquals(1, $output[0]['id']);
        $this->assertEquals(false, $output[0]['complete']);

    }

}